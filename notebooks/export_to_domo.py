from pydomo import Domo
import pandas as pd


def export_dataframe_to_domo(domo: Domo,
                             df_dataset: pd.DataFrame,
                             domo_dataset_name: str,
                             dataset_description: str
                             ):
    """
    Dada una conexión ya establecida con Domo, exporta un dataframe a Domo. Si el nombre del dataset no existe,
    lo crea, si ya existe, lo reescribe.
    ATENCIÓN: esta función puede reescribir cualquier dataset creado con este tipo de conexiones.


    :param domo: conexión con domo ya establecida.
    :param df_dataset: data frame de pandas a exportar. El índice se ignora.
    :param domo_dataset_name: Nombre del dataset a crear o sobre-escribir. Debe tener al menos 20 caracteres.
    :param dataset_description: Descripción del dataset. Se aplica sólo en el caso de creación del dataset. Si el
        dataset ya existe, se ignora. Puede modificarse luego desde DOMO.
    :return: id del dataset (existente o creado).
    """

    name_start = 'PRD_'
    min_len = 20
    if len(domo_dataset_name) < min_len:
        raise ValueError(f"domo_dataset_name debe contener al menos {min_len} caracteres para minimizar colisiones.")
    if domo_dataset_name[:len(name_start)] != name_start:
        raise ValueError(f"domo_dataset_name debe comenzar con {name_start}.")
    df_domo_datasets = domo.ds_list()
    if domo_dataset_name in df_domo_datasets["name"].to_list():
        dataset_id = df_domo_datasets[df_domo_datasets["name"] == domo_dataset_name]['id'].iloc[0]
        domo.ds_update(dataset_id, df_dataset)
    else:
        dataset_id = domo.ds_create(df_dataset, domo_dataset_name, dataset_description)
    return dataset_id
