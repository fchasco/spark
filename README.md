# README #

Spark envirnoment 

- Spark clúster Standalone con 3 workers.

- Jupyter Notebook

Detalle:

spark: Spark Master.

Port: 8282

Image: bitnami/spark:3.2.0
/ Port: 8181

Image: jupyter/pyspark-notebook:spark-3.2.0
/ Port: 8888

# Clonar el Repo #
    
    git clone https://gitlab.com/fchasco/spark.git

# Levantar el Contenedor #

    docker-compose up -d --no-deps

# Configurar el acceso #

    Jupyter Notebook: http://127.0.0.1:8888
    
    
    Spark Master: http://localhost:8181 (No se ejecuta hasta que la session de spark inicie apuntando al puerto 7077)

Para acceder a Jupyter Notebook, por unica vez, se debe copiar el siguiente comando en la consola de Powershell. Esto genera un URL token mediante el cual se obtiene acceso al entorno interactivo. 
Basta con copiar y pegar la url que devuelve consola para comenzar a utilizar Jupyter. 

    docker logs -f docker_jupyter-spark_1
    
    
# Spark Session local

Para visualizar el job 

    http://localhost:4040/jobs/
    
    Hay ejemplos en la carpeta: /home/jovyan/work/notebooks/

# Spark session con nodos esclavos #

    Este ejemplo ejecuta un archivo .py  que hace un conteo de palabras. Spark master hace uso de la configuracion del contenedor (3 workers) y el tercer parametro es la entrada de al funcion word count. Se adjunta un README.md como ejemplo
    
    docker exec -it docker_spark_1 spark-submit --master spark://spark:7077 /usr/local/spark/app/hello-world.py /usr/local/spark/resources/data/README.md
    
    Para ver el detelle del DAG: http://localhost:8181/


### Como aumentar la cantidad de nodos esclavos ##

Se puede aumentar la cantidad de trabajadores de Spark simplemente agregando nuevos servicios basados ​​en la bitnami/spark:latestimagen al docker-compose.ymlarchivo de la siguiente manera:

    spark-worker-n:

        image: bitnami/spark:3.2.0
        
        networks:
            - default_net
            
        environment:
            - SPARK_MODE=worker
            - SPARK_MASTER_URL=spark://spark:7077
            - SPARK_WORKER_MEMORY=1G
            - SPARK_WORKER_CORES=1
            - SPARK_RPC_AUTHENTICATION_ENABLED=no
            - SPARK_RPC_ENCRYPTION_ENABLED=no
            - SPARK_LOCAL_STORAGE_ENCRYPTION_ENABLED=no
            - SPARK_SSL_ENABLED=no
            
        volumes:
            - ../spark/app:/usr/local/spark/app # Spark scripts folder (Must be the same path in airflow and Spark Cluster)
            - ../spark/resources/data:/usr/local/spark/resources/data #Data folder (Must be the same path in airflow and Spark Cluster)
            
## Para abrir una consola de Spark con Python y Scala ###

    Abrir la consola de la maquina "docker_spark_1".
    Comandos:
    
        cd bin
    
    Para pyspark:
        pyspark
        
    Para scala:
        spark-shell

El contendor hace uso de las imagenes oficiales de Spark (bitnami) y Jupyter Notebook (jovyan)

References:

    https://github.com/bitnami/bitnami-docker-spark

    https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-pyspark-notebook


Para utilizar spark con airflow es necesario instalar las dependencias que se encuentan en el archivo docker_airflow.txt, en la imagen custom de airflow.


        
